#pragma once

#include <time.h>
#include <utils/camera.h>

#include "game_object.h"
#include "game_object_handler.h"
#include "physics_handler.h"
#include "hud.h"

class Player
{
public:        
    
    bool drawing = false;                       // True if the player is drawing the bow    
    bool tired = false;                         // True if the player is tired    
    glm::vec2 tired_offset = glm::vec2(0, 0);   // These coordinates are added to mouse movement to simulate player fatigue 

    Player(glm::vec3 initial_position, float initial_rotation, Hud* hud, GameObjectHandler* game_object_handler, vector<GameObject*>* arrows, PhysicsHandler* physics_handler)
    {
        // Store initial position/rotation, used when the game is reset
        this->initial_position = initial_position;
        this->initial_rotation = initial_rotation;
        
        // Wrap camera object
        this->camera = new Camera(initial_position, GL_TRUE, initial_rotation); 
        
        // To update drawing bar and number of arrows
        this->hud = hud;        
        
        // Used to create arrows
        this->game_object_handler = game_object_handler;
        
        // Created arrows will be added to this vector for rendering and physics handling
        this->arrows = arrows;
        
        // Initialize random seed: used to randomize tired effects
        srand(time(NULL));
        
        // Setup initial numer of arrows
        hud->UpdateArrowNumber(arrow_number);
        
        // Setup player rigid body, will be used for collision detection with trees, ground and targets
        body = physics_handler->CreateRigitBody(SPHERE, camera->Position, glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f, 0.0f, 0.0f), 10.0f, 0.3f, 0.0f);        
    }
    
    ~Player()
    {
    }
    
    // Retrieves player position 
    glm::vec3 Position()
    {
        return camera->Position;
    }
    
    // Retrieves view matrix
    glm::mat4 GetViewMatrix()
    {
        return camera->GetViewMatrix();
    }
    
    // To move the player along specified direction (wasd movement)
    void Move(Camera_Movement direction)
    {
        // Activate rigid body, in some situations physics engine set it as ISLAND_SLEEPING (2)
        body->setActivationState(ACTIVE_TAG);
        
        // Blend directions in order to build resulting movement direction 
        if (direction == FORWARD)                 
            velocity_vector += camera->WorldFront;         
        else if (direction == BACKWARD)                 
            velocity_vector -= camera->WorldFront;           
        if (direction == RIGHT)                    
            velocity_vector += camera->Right;           
        else if (direction == LEFT)
            velocity_vector -= camera->Right;          
        
        // Normalize and multiply by speed to calculate final velocity vector
        velocity_vector = camera->MovementSpeed * glm::normalize(velocity_vector);
        UpdateVelocity();
        
        // Update camera position to rigid body position
        UpdateCameraPosition();
    }
    
    // To stop player movement (wasd movement)
    void Stop()
    {
        // Force velocity vector to zero
        velocity_vector = glm::vec3(0, 0, 0);
        UpdateVelocity();        
    }        
    
    // Make the player to look around (mouse movement)
    void LookAt(GLfloat xoffset, GLfloat yoffset)
    {
        camera->ProcessMouseMovement(xoffset, yoffset);
    }
    
    // Start drawing the bow
    void Draw()
    {
        if(arrow_number > 0)
        {
            drawing = true;
            draw_start_time = glfwGetTime();          
            shoot_speed = SHOOT_INITIAL_SPEED;
        }
        else
        {
            hud->GameOver();
        }
    }
    
    // Drawing the bow
    void Hold()
    {
        // Calculate drawing time
        draw_time = glfwGetTime() - draw_start_time;
        
        // The more the drawing time the more the shoot speed
        shoot_speed = SHOOT_INITIAL_SPEED + SHOOT_INITIAL_SPEED * draw_time;
        
        // Shoot speed is clamped to maximum allowed speed
        shoot_speed = shoot_speed > SHOOT_MAX_SPEED ? SHOOT_MAX_SPEED : shoot_speed;       

        // Update drawing bar to make the player understand when shoot reaches the maximum speed 
        // and when the player is becoming tired
        hud->UpdateDrawingBar(draw_time, TIRED_AFTER, shoot_speed >= SHOOT_MAX_SPEED);
        
        // Drawing time too long, the player is tired
        if(draw_time > TIRED_AFTER)
        {    
            // First time tired for this drawing, init stuff
            if(!tired)
            {                
                tired = true;
                // Tick time is renewed after TIRED_UPDATE_OFFSET_AFTER seconds
                tired_tick_time = glfwGetTime();
            } 
           
            // Every each TIRED_UPDATE_OFFSET_AFTER tired coefficients are changed 
            if(glfwGetTime() - tired_tick_time > TIRED_UPDATE_OFFSET_AFTER)
            {
                // Renew tick time
                tired_tick_time = glfwGetTime();    
                
                // x,y coordinates offset randomization is made using A * cos(F * t) formula.
                // "A" and "F" are configured values stored inside TIRED_MULTIPLIER and TIRED_ALPHA_MULTIPLIER arrays.
                // Randomization takes place on index values used to retrieve "A" and "F" components.
                int tired_mult_x = TIRED_MULTIPLIER[rand() % TIRED_MULTIPLIER_SIZE];
                int tired_mult_y = TIRED_MULTIPLIER[rand() % TIRED_MULTIPLIER_SIZE];
                int tired_amult_x = TIRED_ALPHA_MULTIPLIER[rand() % TIRED_ALPHA_MULTIPLIER_SIZE];
                int tired_amult_y = TIRED_ALPHA_MULTIPLIER[rand() % TIRED_ALPHA_MULTIPLIER_SIZE];
                tired_offset.x = tired_mult_x * cos(draw_time * tired_amult_x);
                tired_offset.y = tired_mult_y * cos(draw_time * tired_amult_y);
            }                                                       
        }
    }
    
    // Shoot the arrow
    void Shoot()
    {
        // Release the arrow and update available arrows
        this->Release();
        arrow_number -= 1;
        hud->UpdateArrowNumber(arrow_number);
        std::cout << "Final delta time " << draw_time << ", speed " << shoot_speed << std::endl;        
                
        // Create arrow game object and put inside arrows vector: this vector is used by main program to render and update arrow puhysics
        GameObject* arrow = game_object_handler->CreateArrow(camera);
        arrows->push_back(arrow);                       
    
        // The arrow is shot along Front direction with calculated shoot speed
        glm::vec3 shoot = glm::vec3(camera->Front) * shoot_speed;
        
        // Create an inpulse and apply it to arrow rigid body using the physics engine
        btVector3 impulse = btVector3(shoot.x, shoot.y, shoot.z);        
        arrow->rigid_body->applyCentralImpulse(impulse);               
    }
    
    // Release the arrow. Note that arrow is released even if the player moves while drawing. In this case the arrow is not shot.
    void Release()
    {
        drawing = false;
        tired = false;
        hud->ResetDrawingBar();
    }
    
    void Reset()
    {
        // Reset arrow number
        arrow_number = INITIAL_ARROW_NUMBER;
        arrows->clear();
        
        // Reset HUD data
        hud->Reset(arrow_number);
        
        // Move player rigid body to its initial position
        btVector3 position = btVector3(initial_position.x, initial_position.y, initial_position.z);
        btTransform objTransform;
        objTransform.setIdentity();
        objTransform.setOrigin(position);
        body->setWorldTransform(objTransform);
        body->getMotionState()->setWorldTransform(objTransform);
        
        // Update camera position to rigid body position
        UpdateCameraPosition();         
        // Reset orientation to initial rotation
        camera->ResetOrientation(initial_rotation);
    }
    
private:
    const int INITIAL_ARROW_NUMBER = 10;
    const float TIRED_AFTER = 3;                    // The player becomes tired after 3 seconds
    const float TIRED_UPDATE_OFFSET_AFTER = 0.5f;   // Tired movement is renewed every each 0.5 seconds
    const int SHOOT_INITIAL_SPEED = 10;             
    const int SHOOT_MAX_SPEED = 25;
    
    // Tired effect is made applying to x,y mouse coodinates an offset calculated by A * cos(F * t) formula
    // This array store F components, random function is used to calculate the index of this array
    static const int TIRED_ALPHA_MULTIPLIER_SIZE = 3;
    int TIRED_ALPHA_MULTIPLIER[TIRED_ALPHA_MULTIPLIER_SIZE] = {4, 8, 12};
    // This array store A components, random function is used to calculate the index of this array
    static const int TIRED_MULTIPLIER_SIZE = 2;
    int TIRED_MULTIPLIER[TIRED_MULTIPLIER_SIZE] = {-1, 1};    // 1/-1 to choose between up/down, left/right
        
    vector<GameObject*>* arrows;                    // Vector used by the main program to render and update arrow physics
    Camera* camera;                                 // Camera is updated depending on player rigid body position
    Hud* hud;                                       // Hud is updated depending on player actions 
    GameObjectHandler* game_object_handler;         // Used to create arrows game objects
    btRigidBody* body;                              // Player rigid body, used to handle player collisions with trees, targets and the ground
    
    glm::vec3 initial_position;
    float initial_rotation;    
    glm::vec3 velocity_vector = glm::vec3(0, 0, 0); // Player velocity vector used to move player rigid body    
    int arrow_number = INITIAL_ARROW_NUMBER;        
    float draw_start_time = 0;
    float draw_time = 0;
    float shoot_speed = 0;
    float tired_tick_time = 0;                      
    
    // Update player rigid body velocity
    void UpdateVelocity()
    {
        body->setLinearVelocity(btVector3(velocity_vector.x, velocity_vector.y, velocity_vector.z));
    }
    
    // Update camera position to rigid body position
    void UpdateCameraPosition()
    {
        btVector3 rbpos = body->getCenterOfMassPosition();
        camera->Position = glm::vec3(rbpos.getX(), rbpos.getY()+0.25f, rbpos.getZ());     
    }
    
};

