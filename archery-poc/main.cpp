#include <string>

// Loader estensioni OpenGL
// http://glad.dav1d.de/
// THIS IS OPTIONAL AND NOT REQUIRED, ONLY USE THIS IF YOU DON'T WANT GLAD TO INCLUDE windows.h
// GLAD will include windows.h for APIENTRY if it was not previously defined.
// Make sure you have the correct definition for APIENTRY for platforms which define _WIN32 but don't use __stdcall
#ifdef _WIN32
    #define APIENTRY __stdcall
#endif

#include <glad/glad.h>

// GLFW library to create window and to manage I/O
#include <glfw/glfw3.h>

// Another check related to OpenGL loader,
// confirm that GLAD didn't include windows.h
#ifdef _WINDOWS_
    #error windows.h was included!
#endif

// Classes developed during lab lectures to manage shaders, to load models, for FPS camera, and for physical simulation
#include <utils/shader_v1.h>
#include <utils/model_v2.1.h>
#include <utils/camera.h>
#include <utils/physics_v1.1.h>

// GLM classes 
#include <glm/glm.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

// Image loading
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

// Wrap game object elements, like 3d model, physics rigid body, etc.
#include "game_object.h"
// Head up display 
#include "hud.h"

// Utilities 
// Facade to setup GLFWwindow, also hold window configurations
#include "window_helper.h"
// Holds shaders configurations (fog, skybox, hud)
#include "shader_handler.h"
// Handles player keyboard and mouse inputs
#include "io_handler.h"
// Wrap physics and handles arrow fly model and collisions
#include "physics_handler.h"
// Centralize game object configuration and creation
#include "game_object_handler.h"

// Handle player actions, also wraps the camera
#include "player.h"

// callback functions for keyboard and mouse events
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

IOHandler* io_handler;

////////////////// MAIN function ///////////////////////
int main()
{
    std::cout << "Initializing resources..." << std::endl;
    
    // **************************************************************************************
    // INITIALIZATIONS 
    
    // Init Window, Camera and Shader
    GLFWwindow* window = WindowHelper::InitWindow(key_callback, mouse_callback);
    if(!window)
    {
        return -1;
    }    
    ShaderHandler shader_handler = ShaderHandler();  
    
    // Init HUD
    Hud* hud  = new Hud();
   
    // Init physics handler
    PhysicsHandler* physics_handler = new PhysicsHandler(hud);
    
    // Init Game Object handler
    GameObjectHandler* game_object_handler = new GameObjectHandler(physics_handler);    
    
    // Init forest scene
    GameObject* forest = game_object_handler->CreateForest();    
    
    // Init targets
    vector<GameObject*> target_objs = game_object_handler->CreateTargets();    

    // Init skybox
    GameObject* skybox = game_object_handler->CreateSkybox();
    
    // Init arrows
    vector<GameObject*> arrows;
    
    // Init player
    Player* player = new Player(glm::vec3(26.0f, 0.0f, -26.0f), 135.0f, hud, game_object_handler, &arrows, physics_handler);    
    
    // Init IO handler    
    io_handler = new IOHandler(player, hud, physics_handler);
    
    // **************************************************************************************
    // RENDERING LOOP
    
    std::cout << "Start game loop..." << std::endl;
    GLfloat lastFrame = 0.0f;
    while(!glfwWindowShouldClose(window))
    {      
        // Frame time
        GLfloat currentFrame = glfwGetTime();
        GLfloat deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        
         // "clear" the frame and z buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Show FPS
        hud->UpdateFps(1/deltaTime);

        // Physic simulation
        physics_handler->stepSimulation(deltaTime);      
      
        // IO handling
        glfwPollEvents();
        io_handler->apply(deltaTime);
        
        // View matrix
        glm::mat4 view = player->GetViewMatrix();                              

        // Fog shader, to render forest, targets and arrows
        shader_handler.UseFogShader(view, player->Position()); 
 
        // Forest rendering     
        shader_handler.ActivateAndBindTexture(forest->configuration, shader_handler.fog_shader, false);
        forest->Render(shader_handler.fog_shader, view);
        
        // Targets rendering             
        for(GameObject* target : target_objs)
        {            
            shader_handler.ActivateAndBindTexture(target->configuration, shader_handler.fog_shader, target->hightlight);
            target->Render(shader_handler.fog_shader, view);
        }
      
        // Arrow rendering and logic
        shader_handler.ActivateAndBindTexture(game_object_handler->game_object_cfg.at(GameObject::ARROW), shader_handler.fog_shader, false);
        int size = arrows.size();
        for(int i = 0; i < size; ++i)
        {
            GameObject* go = arrows[i];          
            physics_handler->HandleArrowPhysics(go);
            go->Render(shader_handler.fog_shader, view);            
        }                                    
        
        // Skybox rendering
        // we render it after all the other objects, in order to avoid the depth tests as much as possible.
        // we will set, in the vertex shader for the skybox, all the values to the maximum depth. Thus, the environment map is rendered only where there are no other objects in the image (so, only on the background). Thus, we set the depth test to GL_LEQUAL, in order to let the fragments of the background pass the depth test (because they have the maximum depth possible, and the default setting is GL_LESS)
        glDepthFunc(GL_LEQUAL);
        shader_handler.UseSkyboxShader(view); 
        shader_handler.ActivateAndBindTexture(skybox->configuration, shader_handler.skybox_shader, false);
        skybox->Render(shader_handler.skybox_shader, view);
        // we set again the depth test to the default operation for the next frame
        glDepthFunc(GL_LESS);        

        // Hud rendering
        // Last rendered to allow transparent pixels to use background colors
        glEnable(GL_BLEND);
        shader_handler.UseHudShader();
        hud->Render(shader_handler.hud_shader);                         
        glDisable(GL_BLEND);
      
        // Front and back buffer swap
        glfwSwapBuffers(window);
    }

    // Cleanup
    shader_handler.Delete();
    physics_handler->Clear();
    glfwTerminate();
    return 0;
}


//////////////////////////////////////////
// callback for keyboard events
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    io_handler->key_pressed(window, key, scancode, action, mode);
}

//////////////////////////////////////////
// callback for mouse events
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    io_handler->mouse_moved(window, xpos, ypos);
}

