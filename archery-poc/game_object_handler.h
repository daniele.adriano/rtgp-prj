#pragma once

#import <time.h>
#include <vector>
#include <utils/texture_helper_v1.h>
#include "game_object.h"
#include "physics_handler.h"

class GameObjectHandler
{
    
public:    

    // Holds game object configurations
    std::map<GameObject::Type, GameObject::Configuration> game_object_cfg;

    GameObjectHandler(PhysicsHandler* physics_handler)
    {        
        this->physics_handler = physics_handler;
        SetupConfiguration();         
    }
    
    ~GameObjectHandler()
    {}        
    
    GameObject* CreateForest()
    {                       
        // Forest
        glm::vec3 forest_pos = glm::vec3(0.0f, -1.0f, 0.0f);
        glm::vec3 forest_size = glm::vec3(1.0f, 1.0f, 1.0f);
        glm::vec3 forest_rot = glm::vec3(0.0f, 0.0f, 0.0f);
        GameObject* forest = new GameObject(game_object_cfg.at(GameObject::FOREST), forest_pos, forest_size, forest_rot);
        // This rigid body represent the ground
        physics_handler->CreateRigidBody(forest, BOX, glm::vec3(100.0f, 0.1f, 100.0f), 0.0f, 0.3f, 0.3f);        
        
        // Add trees rigid bodies
        for(int i=0; i<MARPLE_NUMBER; i++)
        {
            glm::vec3 marple_pos = MARPLE_POS[i];
            glm::vec3 marple_size = MARPLE_SIZE[i];
            glm::vec3 marple_rot = glm::vec3(0.0f, 0.0f, 0.0f);        
            physics_handler->CreateRigitBody(CYLINDER, marple_pos, marple_size, marple_rot, 0.0f, 0.3f, 0.3f); 
        }                
        return forest;
    }    
    
    vector<GameObject*> CreateTargets()
    {   
        vector<GameObject*> target_objects; 
        
        // Setup random index position/rotation selector
        vector<int> shuffle_idx;
        for(int i=0; i<CONFIG_NUMBER; i++)
        {
            shuffle_idx.push_back(i);
        }
        // Shuffle target index
        srand(time(0));
        std::random_shuffle(shuffle_idx.begin(), shuffle_idx.end());
        
        // Use random index to select desired target positions/rotations
        for(int i=0; i<TARGET_NUMBER; i++)
        {
            int idx = shuffle_idx[i];                        
            glm::vec3 t1_pos = TARGET_POS[idx];            
            glm::vec3 t1_rot = TARGET_ROT[idx];
            glm::vec3 t1_size = glm::vec3(1.0f, 1.0f, 1.0f);
            GameObject* target = new GameObject(game_object_cfg.at(GameObject::TARGET), t1_pos, t1_size, t1_rot);
            physics_handler->CreateRigidBody(target, BOX, glm::vec3(0.5f, 0.5f, 0.05f), 0.0f, 0.3f, 0.3f);              
            target_objects.push_back(target);        
        }        
        return target_objects;
    } 
    
    GameObject* CreateArrow(Camera* camera)
    {   
        // Arrow initial position is moved along Front vector to avoid collision with player rigid body
        glm::vec3 arrow_pos = camera->Position + (camera->Front * 0.8f);             
        // Arrow rigid body is a thin box, lightly shorter than arrow model 
        glm::vec3 box_size = glm::vec3(0.01f, 0.01f, 0.15f);        
        glm::vec3 arrow_size = glm::vec3(0.2f, 0.2f, 0.2f);
        // Camera yaw - 90 to make the arrow to "point" in the direction of -z axis (additional minus to rotate clockwise)  
        glm::vec3 arrow_rot = glm::vec3(glm::radians(-camera->Yaw-90), glm::radians(camera->Pitch), 0.0f);      

        GameObject* arrow = new GameObject(game_object_cfg.at(GameObject::ARROW), arrow_pos, arrow_size, arrow_rot);             
        physics_handler->CreateRigidBody(arrow, BOX, box_size, 1.3f, 10.0f, 0.0f);
        return arrow;
    }
    
    GameObject* CreateSkybox()
    {            
        GameObject* skybox = new GameObject(game_object_cfg.at(GameObject::SKYBOX));
        return skybox;
    }
    
    
private:
    
    // Game model path configuration
    const char* ARROW_MODEL_PATH = "../../models/arrow/arrow2.obj";
    const char* TARGET_MODEL_PATH = "../../models/target/target.obj";
    const char* FOREST_MODEL_PATH = "../../models/forest/forest.obj";
    const char* MARPLE_MODEL_PATH = "../../models/cylinder.obj";
    const char* SKYBOX_MODEL_PATH = "../../models/cube.obj";
    const char* SKYBOX_TEXTURE_PATH = "../../textures/cube/cloud/";    
    
    // Constant for pi/4 (45 degrees)
    const float pi4 = glm::pi<float>()/4;
    
    // Number of visible targets
    const int TARGET_NUMBER = 4;
    // Number of all possibles targets
    static const int CONFIG_NUMBER = 9;
    // Target position/rotation configurations. Positions coordinates was taken from Blender.
    const glm::vec3 TARGET_POS[CONFIG_NUMBER] = {glm::vec3(-7.5f, -0.5f, -9.0f), glm::vec3(-23.0f, -0.5f, -28.5f),   glm::vec3(-31.2f, -0.5f, -10.5f),    glm::vec3(-28.3f, -0.5f, 19.2f),    glm::vec3(-13.5f, -0.5f, 31.0f),   glm::vec3(15.0f, -0.5f, 0.5f),    glm::vec3(19.5f, -0.5f, 33.0f),   glm::vec3(31.0f, -0.5f, -4.5f),     glm::vec3(6.2f, -0.5f, -31.0f)};
    const glm::vec3 TARGET_ROT[CONFIG_NUMBER] = {glm::vec3(-pi4, 0.0f, 0.0f),    glm::vec3(pi4, 0.0f, 0.0f),         glm::vec3(-pi4, 0.0f, 0.0f),         glm::vec3(-pi4, 0.0f, 0.0f),        glm::vec3(0.0f, 0.0f, 0.0f),       glm::vec3(pi4, 0.0f, 0.0f),       glm::vec3(pi4, 0.0f, 0.0f),       glm::vec3(pi4, 0.0f, 0.0f),         glm::vec3(pi4, 0.0f, 0.0f)};
    
    // These configurations are used to create trees rigid bodies (cylinder). Positions coordiates was taken from Blender.
    // Number of trees
    static const int MARPLE_NUMBER = 12;
    // Trees positions
    const glm::vec3 MARPLE_POS[MARPLE_NUMBER] = {
            glm::vec3(6.24486f, 2.0f, -29.8863f), 
            glm::vec3(-24.457f, 2.0f, -29.8863f),
            glm::vec3(-31.2553f, 2.0f, -8.72798f),
            glm::vec3(-8.32235f, 2.0f, -10.177f),
            glm::vec3(14.6414f, 2.0f, 1.52363f),            
            glm::vec3(31.2909f, 2.0f, -4.17325f),
            glm::vec3(-28.3229f, 2.0f, 17.2271f),
            glm::vec3(-12.0162f, 2.0f, 31.038f),
            glm::vec3(18.2547f, 2.0f, 33.3376f),
            // big trees
            glm::vec3(27.5153f, 4.0f, 19.5424f),
            glm::vec3(28.7829f, 4.0f, -20.0665f),
            glm::vec3(-10.2608f, 4.0f, 7.54694f)
    };
    // Trees size
    const glm::vec3 MARPLE_SIZE[MARPLE_NUMBER] = {
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            glm::vec3(0.3f, 3.0f, 0.3f), 
            // big trees
            glm::vec3(0.8f, 5.0f, 0.8f), 
            glm::vec3(0.8f, 5.0f, 0.8f), 
            glm::vec3(0.8f, 5.0f, 0.8f), 
    };
    
    // Used to create rigid bodies
    PhysicsHandler* physics_handler;
    
    // Setup game object configurations.
    // Note that there is one configuration for each game object type
    void SetupConfiguration()
    {
        // ARROW
        GameObject::Configuration arrow_cfg;               
        arrow_cfg.type = GameObject::ARROW;
        arrow_cfg.model = new Model(ARROW_MODEL_PATH);
        this->game_object_cfg.insert(pair<GameObject::Type, GameObject::Configuration>(arrow_cfg.type, arrow_cfg));
        
        // TARGET
        GameObject::Configuration target_cfg;        
        target_cfg.type = GameObject::TARGET;
        target_cfg.model = new Model(TARGET_MODEL_PATH);
        this->game_object_cfg.insert(pair<GameObject::Type, GameObject::Configuration>(target_cfg.type, target_cfg));
        
        // FOREST
        GameObject::Configuration forest_cfg = GameObject::Configuration();
        forest_cfg.type = GameObject::FOREST;
        forest_cfg.model = new Model(FOREST_MODEL_PATH);        
        this->game_object_cfg.insert(pair<GameObject::Type, GameObject::Configuration>(forest_cfg.type, forest_cfg));              
        
        // SKYBOX
        GameObject::Configuration skybox_cfg;        
        skybox_cfg.type = GameObject::SKYBOX;
        skybox_cfg.model = new Model(SKYBOX_MODEL_PATH);
        skybox_cfg.self_textured = false;
        skybox_cfg.texture_id = TextureHelper::LoadTextureCube(SKYBOX_TEXTURE_PATH);
        skybox_cfg.texture_type = GL_TEXTURE_CUBE_MAP;
        this->game_object_cfg.insert(pair<GameObject::Type, GameObject::Configuration>(skybox_cfg.type, skybox_cfg));   
    }        
    
};



