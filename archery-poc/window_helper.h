#pragma once

namespace WindowHelper
{
    // Window configurations
    const char* TITLE = "Archery POC";    
    const GLuint SCREEN_WIDTH = 1024;
    const GLuint SCREEN_HEIGHT = 768;
    
    GLFWwindow* InitWindow(GLFWkeyfun keyboard_callback, GLFWcursorposfun mouse_callback)
    {        
        // Initialization of OpenGL context using GLFW
        glfwInit();
        // We set OpenGL specifications required for this application
        // In this case: 3.3 Core
        // It is possible to raise the values, in order to use functionalities of OpenGL 4.x
        // If not supported by your graphics HW, the context will not be created and the application will close
        // N.B.) creating GLAD code to load extensions, try to take into account the specifications and any extensions you want to use,
        // in relation also to the values indicated in these GLFW commands
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        // we set if the window is resizable
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
        
        GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, TITLE, nullptr, nullptr);
        if (!window)
        {
            std::cout << "Failed to create GLFW window" << std::endl;
            glfwTerminate();
            return nullptr;
        }
        glfwMakeContextCurrent(window);
        glfwSetKeyCallback(window, keyboard_callback);
        glfwSetCursorPosCallback(window, mouse_callback);

        // disable the mouse cursor
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        // GLAD tries to load the context set by GLFW
        if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        {
            std::cout << "Failed to initialize OpenGL context" << std::endl;
            return nullptr;
        }

        // viewport dimensions
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);
        
        // Enable blending required for text
        //glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // enable Z test
        glEnable(GL_DEPTH_TEST);

        //the "clear" color for the frame buffer
        glClearColor(0.26f, 0.46f, 0.98f, 1.0f);
        
        return window;
    }

};

