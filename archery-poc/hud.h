#pragma once

#include <utils/text_v1.h>
#include "window_helper.h"

class Hud
{
public:
    
    // To show or hide the HUD 
    bool show = true;
    
    Hud()
    {
        this->text = new Text("../../fonts/arial.ttf");
        
        // Initialize dynamic text
        SetupStringStream(last_score_ss, LAST_SCORE_LABEL, 0);
        SetupStringStream(last_distance_ss, LAST_DISTANCE_LABEL, 0);
        SetupStringStream(total_score_ss, TOTAL_SCORE_LABEL, 0);
        SetupStringStream(arrow_number_ss, ARROW_NUMBER_LABEL, 0);
        SetupStringStream(fps_ss, FPS_LABEL, 0);        
        SetupStringStream(drawing_bar_foreground_ss, "");
        SetupStringStream(game_over_ss, "");
        // Initialize drawing bar
        for(int i=0; i<DRAWING_BAR_SIZE; i++)
        {
            drawing_bar_background_ss << DRAWING_BAR_CHARACTER;
        }
        drawing_bar_color = YELLOW;        
    }
    
    ~Hud()
    {
    }
    
    void UpdateArrowNumber(int arrow_number)
    {
        SetupStringStream(arrow_number_ss, ARROW_NUMBER_LABEL, arrow_number);        
    }
    
    void UpdateScore(int score, int distance)
    {
        last_score = score;
        total_score += score;        
        SetupStringStream(last_score_ss, LAST_SCORE_LABEL, last_score);
        SetupStringStream(last_distance_ss, LAST_DISTANCE_LABEL, distance);
        SetupStringStream(total_score_ss, TOTAL_SCORE_LABEL, total_score);
    }
    
    void UpdateFps(float fps)
    {
        SetupStringStream(fps_ss, FPS_LABEL, fps);
    }
    
    void UpdateDrawingBar(float draw_time, float tired_after, bool full_power)
    {
        // Drawing bar reached the end, player is tired, drawing bar color turns to red
        if(drawing_bar_foreground_ss.tellp() > DRAWING_BAR_SIZE-1)
        {
            drawing_bar_color = RED;
            return;
        }          
        // Shoot power reached the maximum value, drawing bar color turns to green
        if(full_power)
        {
            drawing_bar_color = GREEN;
        }
        // Number of drawing bar characters per second
        // Note that if the drawing bar is full then the player is tired
        float character_per_second = DRAWING_BAR_SIZE / tired_after;     
        // Number of drawing bar characters 
        float character_number = draw_time * character_per_second;
        // Setup drawing bar
        SetupStringStream(drawing_bar_foreground_ss, "");        
        for(int i=0; i<character_number; i++)
        {                
            drawing_bar_foreground_ss << DRAWING_BAR_CHARACTER;                
        }                  
    }
    
    // Arrow is released, set drawing bar initial value, drawing bar color turn to yellow
    void ResetDrawingBar()
    {        
        SetupStringStream(drawing_bar_foreground_ss, "");
        drawing_bar_color = YELLOW;
    }
    
    void GameOver()
    {
        // Build game over message
        SetupStringStream(game_over_ss, "");
        game_over_ss << GAME_OVER_LABEL1 << total_score << GAME_OVER_LABEL2;
        
        // To make game over message visible
        game_over = true;
    }
    
    void Reset(int arrow_number)
    {
        // Reset score
        total_score = 0;
        UpdateScore(0, 0);
        
        // Reset drawing bar
        ResetDrawingBar();
        
        // Set arrow number to initial arrow number
        UpdateArrowNumber(arrow_number);        
        
        // Reset game over message
        SetupStringStream(game_over_ss, "");
        game_over = false;
    }
    
    void Render(Shader &shader)
    {
        if(show)
        {
            // shoot indicator, 0m
            text->Render(shader, SHOOT_INDICATOR, WindowHelper::SCREEN_WIDTH/2-6, WindowHelper::SCREEN_HEIGHT/2-5, SHOOT_INDICATOR_FONT_SIZE, WHITE);
            text->Render(shader, SHOOT_INDICATOR_0, WindowHelper::SCREEN_WIDTH/2+5, WindowHelper::SCREEN_HEIGHT/2-10, SHOOT_INDICATOR_FONT_SIZE, WHITE);
            
            // shoot indicator, 5m
            text->Render(shader, SHOOT_INDICATOR, WindowHelper::SCREEN_WIDTH/2-6, WindowHelper::SCREEN_HEIGHT/2-40, SHOOT_INDICATOR_FONT_SIZE, WHITE);            
            
            // shoot indicator, 10m
            text->Render(shader, SHOOT_INDICATOR, WindowHelper::SCREEN_WIDTH/2-6, WindowHelper::SCREEN_HEIGHT/2-85, SHOOT_INDICATOR_FONT_SIZE, WHITE);        
            text->Render(shader, SHOOT_INDICATOR_10, WindowHelper::SCREEN_WIDTH/2+5, WindowHelper::SCREEN_HEIGHT/2-90, SHOOT_INDICATOR_FONT_SIZE, WHITE);
            
            // shoot indicator, 15m
            text->Render(shader, SHOOT_INDICATOR, WindowHelper::SCREEN_WIDTH/2-6, WindowHelper::SCREEN_HEIGHT/2-135, SHOOT_INDICATOR_FONT_SIZE, WHITE);
            
            // shoot indicator, 20m
            text->Render(shader, SHOOT_INDICATOR, WindowHelper::SCREEN_WIDTH/2-6, WindowHelper::SCREEN_HEIGHT/2-190, SHOOT_INDICATOR_FONT_SIZE, WHITE);
            text->Render(shader, SHOOT_INDICATOR_20, WindowHelper::SCREEN_WIDTH/2+5, WindowHelper::SCREEN_HEIGHT/2-195, SHOOT_INDICATOR_FONT_SIZE, WHITE);
            
            // shoot indicator, 25m
            text->Render(shader, SHOOT_INDICATOR, WindowHelper::SCREEN_WIDTH/2-6, WindowHelper::SCREEN_HEIGHT/2-250, SHOOT_INDICATOR_FONT_SIZE, WHITE);
            
            // Scores
            text->Render(shader, last_distance_ss.str(), WindowHelper::SCREEN_WIDTH-170, 25, FONT_SIZE, WHITE);            
            text->Render(shader, last_score_ss.str(), 25, 25, FONT_SIZE, WHITE);
            text->Render(shader, total_score_ss.str(), WindowHelper::SCREEN_WIDTH/2-70, WindowHelper::SCREEN_HEIGHT-25, FONT_SIZE, WHITE);
            
            // Number of arrows
            text->Render(shader, arrow_number_ss.str(), 25, WindowHelper::SCREEN_HEIGHT-25, FONT_SIZE, WHITE);
            
            // FPS
            text->Render(shader, fps_ss.str(), WindowHelper::SCREEN_WIDTH-100, WindowHelper::SCREEN_HEIGHT-25, FONT_SIZE, BLUE);
            
            // Draw bar
            // foreground
            text->Render(shader, drawing_bar_foreground_ss.str(), WindowHelper::SCREEN_WIDTH/2-155, 25, FONT_SIZE, drawing_bar_color);
            // background
            text->Render(shader, drawing_bar_background_ss.str(), WindowHelper::SCREEN_WIDTH/2-155, 25, FONT_SIZE, WHITE);            
            
            // Game over
            if(game_over)
            {
                text->Render(shader, GAME_OVER_LABEL0, WindowHelper::SCREEN_WIDTH/2-55, WindowHelper::SCREEN_HEIGHT/2+60, FONT_SIZE, BLUE);
                text->Render(shader, game_over_ss.str(), WindowHelper::SCREEN_WIDTH/2-310, WindowHelper::SCREEN_HEIGHT/2+30, FONT_SIZE, BLUE);                        
            }
        }
    }
    
private:
    // Text colors
    const glm::vec3 WHITE = glm::vec3(1, 1, 1);
    const glm::vec3 GREEN = glm::vec3(0.5f, 0.8f, 0.2f);
    const glm::vec3 BLUE = glm::vec3(0.3f, 0.7f, 0.9f);
    const glm::vec3 RED = glm::vec3(1.0f, 0.0f, 0.0f);
    const glm::vec3 YELLOW = glm::vec3(1.0f, 1.0f, 0.0f);
    
    // Font size
    const float FONT_SIZE = 0.4f;
    const float SHOOT_INDICATOR_FONT_SIZE = 0.25f;    
    
    // Label and static text
    const std::string FPS_LABEL = "FPS: ";
    const std::string ARROW_NUMBER_LABEL = "Arrows: ";
    const std::string LAST_SCORE_LABEL =  " Last score: ";
    const std::string LAST_DISTANCE_LABEL =  " Last distance: ";
    const std::string TOTAL_SCORE_LABEL = "Total score: ";        
    const std::string SHOOT_INDICATOR = "_";
    const std::string SHOOT_INDICATOR_0 = "0";
    const std::string SHOOT_INDICATOR_10 = "10";
    const std::string SHOOT_INDICATOR_20 = "20";
    const std::string DRAWING_BAR_CHARACTER = "=";
    const std::string GAME_OVER_LABEL0 = "GAME OVER";
    const std::string GAME_OVER_LABEL1 = "You realized ";
    const std::string GAME_OVER_LABEL2 = " points, press 'R' key to play current level again or 'ESC' to exit";
    
    // Number of elements of drawing bar
    const int DRAWING_BAR_SIZE = 27;
    
    // Dynamic text
    std::stringstream fps_ss;    
    std::stringstream arrow_number_ss;    
    std::stringstream last_score_ss;
    std::stringstream last_distance_ss;
    std::stringstream total_score_ss;
    std::stringstream drawing_bar_foreground_ss;
    std::stringstream drawing_bar_background_ss;
    std::stringstream game_over_ss;
    
    // Utility class to render text
    Text* text;
    
    // Drawing bar color (switch from yellow to green to red)
    glm::vec3 drawing_bar_color;
    
    // Score of the last shoot
    int last_score = 0;
    // Total score
    int total_score = 0;
    // To show game over text
    bool game_over = false;
    
    // Setup dynamic text with specified string
    void SetupStringStream(std::stringstream &ss, std::string text)
    {
        ss.str("");
        ss.clear();
        ss << text;
    }
    
    // Setup dynamic text with specified string and numeric value (eg. "score 10")
    void SetupStringStream(std::stringstream &ss, std::string text, int value)
    {
        SetupStringStream(ss, text);
        ss << std::to_string(value);
    }

};



