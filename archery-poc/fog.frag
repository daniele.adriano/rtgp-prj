#version 330 core

layout(location = 0) out vec4 out_color;
 
uniform sampler2D texture_diffuse1;
uniform float repeat;
uniform bool hightlight;
uniform vec3 player_position; 
uniform vec3 light_direction; 
uniform vec3 light_base_color;   
uniform vec3 rim_base_color;        
uniform vec3 hightlight_base_color;
uniform vec3 fog_base_color;
uniform float fog_density;
 
// From vertex shader, fragment data
in vec3 world_position;       // fragment position in world coordinates
in vec3 world_normal;         // fragment normal vector 
in vec4 view_position;        // fragment position in view coordinates
in vec2 interp_UV;            // fragment UV coordinates
  
void main(){  

  // Repeat is used to both:
  //  1. repeat UVs and sample texture color (repeat > 0)
  //  2. use interpolated UV without repetition (repeat is 0). This is required if texture wrap parameter is set 
  //     to GL_CLAMP_TO_EDGE because in this case UV ranges are changed to [1/2N, 1 - 1/2N].
  //     For details see https://www.khronos.org/registry/OpenGL-Refpages/es2.0/xhtml/glTexParameter.xml
  vec2 uv = repeat != 0 ? mod(interp_UV * repeat, 1.0) : interp_UV;

  // Pick up texture color
  vec4 surface_color_rgba = texture(texture_diffuse1, uv);

  // Discard transparent pixels
  if(surface_color_rgba.a < 0.1) 
    discard;
 
  // Surface color RGB
  vec3 surface_color = surface_color_rgba.rgb;
   
  // Normalize directional light vector
  vec3 L = normalize(light_direction);

  // Normalize vector from the pixel to the view
  vec3 V = normalize(player_position - world_position);

  // Light color component, max base color contribution if the angle between light and normal vectors is 0
  vec3 light_color = light_base_color * max(0, dot(L, world_normal));
      
  // Rim factor, contribution is lesser if the angle between V and normal vectors is 0. 
  // The face in front of the player has no rim contribution, the greater the angle the darker the color.
  float rim_factor = 1 - max(0, dot(V, world_normal));
  rim_factor = smoothstep(0.6, 1.0, rim_factor);
  vec3 rim_color = rim_base_color * rim_factor;//vec3(rim_factor, rim_factor, rim_factor);

  // Surfce color with both rim and light components
  surface_color = rim_color + light_color + surface_color;
   
  // Distance between fragment position and player (view) position
  float dist = length(view_position);

  vec3 final_color = vec3(0, 0, 0);

  /*
  // Exponential fog
  // Calculate for factor
  float fog_factor = 1.0 / exp(dist * fog_density);
  fog_factor = clamp( fog_factor, 0.0, 1.0);

  // Calculate final color mixing for factor and surface color
  vec3 final_color = vec3(0, 0, 0);
  final_color = mix(fog_base_color, surface_color, fog_factor);
  */
  
  // Atmospheric fog 
  // Attenuation component
  float ba = (50 - view_position.y) * 0.002;
  float attenuation = exp(-dist * ba);
  // in_scattering component
  float bs = (50 - view_position.y) * 0.0005;
  float in_scattering = exp(-dist * bs);
  // Final color = surface_color * e^(-dist * ba) + fog_base_color * (1 - e^(-dist * bs))
  final_color = surface_color * attenuation + fog_base_color * (1 - in_scattering);
  

  // Highlight pixel color
  if(hightlight)
  {
    final_color = mix(final_color, hightlight_base_color, 0.2f);
  }
   
  out_color = vec4(final_color, 1);
  
}
