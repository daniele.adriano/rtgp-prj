#version 330 core

layout(location = 0) out vec4 out_color;

uniform sampler2D text;
uniform vec3 textColor;

in vec2 interp_UV;

void main()
{    
	// Use red color as alpha channel because inside generated font textures characters are red
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, interp_UV).r);

    // Use provided color as final text color
    out_color = vec4(textColor, 1.0) * sampled;
}  