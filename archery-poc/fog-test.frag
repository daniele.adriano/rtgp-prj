#version 330 core

layout(location = 0) out vec4 out_color;
 
uniform sampler2D texture_light_color1;
uniform float repeat;
uniform bool hightlight;
uniform vec3 viewPosition;

// TODO remove
uniform sampler2D texture_normal1;

const vec3 light_direction = vec3(0.8, 1, 0); 
const vec3 light_base_color = vec3(1.0, 0.0, 0.0);//vec3(0.15, 0.05, 0.0);   //vec3(1.0, 0.0, 0.0);
const vec3 rim_base_color = vec3(0.0, 0.0, 1.0);//vec3(0.2, 0.2, 0.2);      //vec3(0.0, 0.0, 1.0);
const vec3 hightlight_base_color = vec3(0, 1, 0);
const vec3 fog_base_color = vec3(0.5, 0.5, 0.5);
const float fog_density = 0.05;
 
// From vertex shader
in vec3 world_pos;
in vec3 world_normal;
in vec4 view_pos;
in vec2 interp_UV;
  
void main(){  

  // Repeat is used to both:
  //  1. repeat UVs and sample texture color (repeat > 0)
  //  2. use interpolated UV without repetition (repeat is 0). This is required if texture wrap parameter is set 
  //     to GL_CLAMP_TO_EDGE because in this case UV ranges are changed to [1/2N, 1 - 1/2N].
  //     For details see https://www.khronos.org/registry/OpenGL-Refpages/es2.0/xhtml/glTexParameter.xml
  vec2 uv = repeat != 0 ? mod(interp_UV * repeat, 1.0) : interp_UV;

  // TODO remove
  vec3 world_normal2 = texture(texture_normal1, uv).rgb;
  world_normal2 = normalize(world_normal2 * 2.0 - 1.0);

  // Pick up texture color
  vec4 surface_color_rgba = texture(texture_light_color1, uv);

  // Discard transparent pixels
  if(surface_color_rgba.a < 0.1) 
    discard;
 
  // Surface color RGB
  vec3 surface_color = surface_color_rgba.rgb;

  // Highlight pixel color
  if(hightlight)
  {
    surface_color = mix(surface_color, hightlight_base_color, 0.5f);
  }

  // vec3 lightDir = normalize(light.position - FragPos);
  vec3 lightDir = normalize(-light_direction);  
  float diff = max(dot(world_normal2, lightDir), 0.0);
  vec3 diffuse = vec3(0.5, 0.5, 0.5) * diff * texture(texture_light_color1, uv).rgb;  
  
  // specular
  vec3 viewDir = normalize(view_pos.xyz - world_pos);
  vec3 reflectDir = reflect(-lightDir, world_normal2);  
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
  vec3 specular = vec3(1.0, 1.0, 1.0) * spec * texture(texture_light_color1, uv).rgb;  
      
  vec3 result = surface_color + diffuse + specular;
  out_color = vec4(result, 1.0);




   /*
  // Normalize directional light vector
  vec3 L = normalize(-light_direction);

  // Normalize vector from the pixel to the view
  vec3 V = normalize(viewPosition - world_pos);

  // Light color component, max base color contribution if the angle between light and normal vectors is 0
  vec3 light_color = light_base_color * max(0, dot(L, world_normal2));
      
  // Rim factor, contribution is lesser if the angle between V and normal vectors is 0. 
  // The face in front of the player has no rim contribution, the greater the angle the darker the color.
  float rim_factor = 1 - max(dot(V, world_normal2), 0.0);
  rim_factor = smoothstep(0.6, 1.0, rim_factor);
  vec3 rim_color = rim_base_color * vec3(rim_factor, rim_factor, rim_factor);

  // Surfce color with both rim and light components
  surface_color = rim_color + light_color + surface_color;
   
  // Distance from the view
  float dist = length(view_pos);

  // Calculate for factor
  float fog_factor = 1.0 / exp(dist * fog_density);
  fog_factor = clamp( fog_factor, 0.0, 1.0);

  // Calculate final color mixing for factor and surface color
  vec3 final_color = vec3(0, 0, 0);
  final_color = mix(fog_base_color, surface_color, fog_factor);

  out_color = vec4(final_color, 1);*/
  
}
