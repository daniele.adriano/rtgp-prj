#version 330 core

// vertex position in world coordinates
layout (location = 0) in vec4 position;

out vec2 interp_UV;

uniform mat4 projection_matrix;

void main()
{
	interp_UV = position.zw;

	// Z axis set to 0 becase hud is "attached" to the screen
    gl_Position = projection_matrix * vec4(position.xy, 0.0, 1.0);
    
}  