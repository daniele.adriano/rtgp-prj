#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <utils/physics_v1.1.h>
#include <utils/model_v2.1.h>

class GameObject
{
public:
    
    // Game object type
    enum Type
    {
        ARROW,
        TARGET,
        FOREST,
        SKYBOX
    }; 
    
    // Game object configuration
    struct Configuration
    {
        Type type;
        Model* model;        
        // If true model textures are handled directly by the model class (case of obj + mtl files)
        bool self_textured = true;
        // Configuration below should be set only for not self textured game objects
        // Textures ID, loaded by game object handler and passed to the shader by the shader helper
        GLint texture_id;
        // Define texture repetition
        GLint texture_repeat = 0;
        // Default texture type. This value is changed for example for skybox
        GLint texture_type = GL_TEXTURE_2D;            
    };
    
    // Game object attributes
    Configuration configuration;
    btRigidBody* rigid_body;    
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 size;    
    // Position when the game object is created
    glm::vec3 initial_position;
    // Used by the fragment shader to highlight game object model
    bool hightlight = false;
    
    // This constructor is used by skybox where position/size/rotation are not needed
    GameObject(Configuration configuration)
    {
        this->configuration = configuration;
        this->size = glm::vec3(1, 1, 1);
        this->roto_translation_matrix = glm::mat4();
        this->rigid_body = nullptr;
    }

    // Common game object constructor
    GameObject(Configuration configuration, glm::vec3 position, glm::vec3 size, glm::vec3 rotation)
    {
        this->configuration = configuration;
        this->position = position;
        this->initial_position = position;
        this->size = size;
        this->rotation = rotation;
        this->rigid_body = nullptr;
        
        // Initial rototranslation matrix, used for static objects
        roto_translation_matrix = glm::translate(roto_translation_matrix, position);   
        if(rotation.z != 0)
            roto_translation_matrix = glm::rotate(roto_translation_matrix, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));         
        if(rotation.y != 0)
            roto_translation_matrix = glm::rotate(roto_translation_matrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));    
        if(rotation.x != 0)  
            roto_translation_matrix = glm::rotate(roto_translation_matrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));      
    }
    
    ~GameObject()
    {
    }        
    
    // Render the game object using specified shader/view matrix. 
    // If the game object is linked to a rigid body then the game object rototranslation matrix is retrieved from the physics engine.
    void Render(Shader shader, glm::mat4 view_matrix)
    {       
        if(rigid_body && rigid_body->getMotionState())
        {
            // Retrieve rototranslation matrix from physics engine
            GLfloat rigid_body_model_matrix[16];
            btTransform transform;
            rigid_body->getMotionState()->getWorldTransform(transform);
            transform.getOpenGLMatrix(rigid_body_model_matrix);
            roto_translation_matrix = glm::make_mat4(rigid_body_model_matrix);
        }
        Render(shader, view_matrix, roto_translation_matrix);        
    }
    
    // Render the game object using specified shader/view matrix/model rototranslation matrix
    void Render(Shader shader, glm::mat4 view_matrix, glm::mat4 roto_translation_matrix)
    {
        glm::mat4 model_matrix = glm::scale(roto_translation_matrix, size);         
        glm::mat3 normal_matrix = glm::inverseTranspose(glm::mat3(view_matrix*model_matrix));
        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "modelMatrix"), 1, GL_FALSE, glm::value_ptr(model_matrix));
        glUniformMatrix3fv(glGetUniformLocation(shader.Program, "normalMatrix"), 1, GL_FALSE, glm::value_ptr(normal_matrix));
        configuration.model->Draw(shader);
    }

private:

    // Model rototranslation matrix, if a rigid body is linked the the game object then this matrix is retrieved from the physics engine
    glm::mat4 roto_translation_matrix;

};



