#pragma once

#include "window_helper.h"

class ShaderHandler
{
public:

    Shader fog_shader;
    Shader skybox_shader;
    Shader hud_shader;
    
    ShaderHandler()
    {
        // Shader initialization
        fog_shader = Shader(SHADER_FOG_VERT, SHADER_FOG_FRAG);
        skybox_shader = Shader(SHADER_SKYBOX_VERT, SHADER_SKYBOX_FRAG);
        hud_shader = Shader(SHADER_HUD_VERT, SHADER_HUD_FRAG);
    }
    
    ~ShaderHandler() 
    {
    }
    
    void Delete()
    {
        // Cleanup
        fog_shader.Delete();
        skybox_shader.Delete();
        hud_shader.Delete();
    }
    
    void UseFogShader(glm::mat4 view_matrix, glm::vec3 player_position)
    {               
        fog_shader.Use();
        
        // Player view position
        glUniform3fv(glGetUniformLocation(fog_shader.Program, "player_position"), 1, glm::value_ptr(player_position));     
        
        // Direct light configurations
        glUniform3fv(glGetUniformLocation(fog_shader.Program, "light_direction"), 1, glm::value_ptr(light_direction));     
        glUniform3fv(glGetUniformLocation(fog_shader.Program, "light_base_color"), 1, glm::value_ptr(light_base_color));     
        
        // Fog configurations
        glUniform3fv(glGetUniformLocation(fog_shader.Program, "rim_base_color"), 1, glm::value_ptr(rim_base_color));             
        glUniform3fv(glGetUniformLocation(fog_shader.Program, "fog_base_color"), 1, glm::value_ptr(fog_base_color));     
        
        // Hightligh color, used to highlight hit targets
        glUniform3fv(glGetUniformLocation(fog_shader.Program, "hightlight_base_color"), 1, glm::value_ptr(hightlight_base_color));     
        
        // View and projection matrixes
        glUniformMatrix4fv(glGetUniformLocation(fog_shader.Program, "projection_matrix"), 1, GL_FALSE, glm::value_ptr(PROJECTION_MATRIX));
        glUniformMatrix4fv(glGetUniformLocation(fog_shader.Program, "view_matrix"), 1, GL_FALSE, glm::value_ptr(view_matrix));
    }
    
    void UseSkyboxShader(glm::mat4 view_matrix)
    {                
        skybox_shader.Use();        
        // we pass projection and view matrices to the Shader Program of the skybox
        glUniformMatrix4fv(glGetUniformLocation(skybox_shader.Program, "projection_matrix"), 1, GL_FALSE, glm::value_ptr(PROJECTION_MATRIX));
        // to have the background fixed during camera movements, we have to remove the translations from the view matrix
        // thus, we consider only the top-left submatrix, and we create a new 4x4 matrix
        glm::mat4 view = glm::mat4(glm::mat3(view_matrix));    // Remove any translation component of the view matrix
        glUniformMatrix4fv(glGetUniformLocation(skybox_shader.Program, "view_matrix"), 1, GL_FALSE, glm::value_ptr(view));        
    }
    
    void UseHudShader()
    {
        hud_shader.Use();
        // Note that hud shader has dedicated projection matrix (orthogonal projection matrix)
        glUniformMatrix4fv(glGetUniformLocation(hud_shader.Program, "projection_matrix"), 1, GL_FALSE, glm::value_ptr(HUD_PROJECTION_MATRIX));        
    }
    
    void ActivateAndBindTexture(GameObject::Configuration game_object_cfg, Shader shader, bool hightlight)
    {        
        // Texture repeat
        glUniform1f(glGetUniformLocation(shader.Program, "repeat"), game_object_cfg.texture_repeat);        
        
        // To highlight game object texture
        glUniform1f(glGetUniformLocation(shader.Program, "hightlight"), hightlight);
        
        // Activate and bind not self textured game objects
        if(!game_object_cfg.self_textured) 
        {
            glUniform1i(glGetUniformLocation(shader.Program, "texture_diffuse1"), 0);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(game_object_cfg.texture_type, game_object_cfg.texture_id);               
        }
    }
    
private:        
    
    // Projection matrix configurations
    const GLfloat FOV_ANGLE = 45.0f;
    const GLfloat NEAR_PLANE = 0.1f;
    const GLfloat FAR_PLANE = 10000.0f;    
    const glm::mat4 PROJECTION_MATRIX = glm::perspective(FOV_ANGLE, (float)WindowHelper::SCREEN_WIDTH/(float)WindowHelper::SCREEN_HEIGHT, NEAR_PLANE, FAR_PLANE);
    
    // HUD projection matrix
    const glm::mat4 HUD_PROJECTION_MATRIX = glm::ortho(0.0f, (float)WindowHelper::SCREEN_WIDTH, 0.0f, (float)WindowHelper::SCREEN_HEIGHT);
    
    // Fog Shader    
    const char* SHADER_FOG_VERT = "fog.vert";
    const char* SHADER_FOG_FRAG = "fog.frag";
    
    const glm::vec3 light_direction = glm::vec3(0.8, 1, 0);           // direct light vector
    const glm::vec3 light_base_color = glm::vec3(0.15, 0.05, 0.0);    // light color - vec3(0.0, 1.0, 0.0); for testing purposes, original vec3(0.15, 0.05, 0.0); 
    const glm::vec3 rim_base_color = glm::vec3(0.2, 0.2, 0.2);        // rim color - vec3(1.0, 0.0, 0.0); for testing purposes, original vec3(0.2, 0.2, 0.2);
    const glm::vec3 hightlight_base_color = glm::vec3(0, 1, 0);       // highlighted object color (target becomes green)
    const glm::vec3 fog_base_color = glm::vec3(0.5, 0.5, 0.5);        // fog color - vec3(0.0, 0.0, 1.0); for testing purposes, original vec3(0.5, 0.5, 0.5)
    
    // Skybox shader
    const char* SHADER_SKYBOX_VERT = "skybox.vert";
    const char* SHADER_SKYBOX_FRAG = "skybox.frag";
    
    // HUD hader 
    const char* SHADER_HUD_VERT = "hud.vert";
    const char* SHADER_HUD_FRAG = "hud.frag";
    
};
