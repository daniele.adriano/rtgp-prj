#pragma once

#include "hud.h"
#include "player.h"
#include "physics_handler.h"

class IOHandler
{
public:

    IOHandler(Player* player, Hud* hud, PhysicsHandler* physics_handler)
    {
        // To control player movement/look/shoot and to handle game reset
        this->player = player;        
        // To show / hide hud
        this->hud = hud;
        // To handle game reset
        this->physics_handler = physics_handler;
    }
    ~IOHandler()
    {
    }    

    //////////////////////////////////////////
    // callback for keyboard events
    void key_pressed(GLFWwindow* window, int key, int scancode, int action, int mode)
    {
        // if ESC is pressed, we close the application
        if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GL_TRUE);

        // if L is pressed, we activate/deactivate wireframe rendering of models
        if(key == GLFW_KEY_L && action == GLFW_PRESS)
            wireframe=!wireframe;            
        
        // Show / hide Hud with H key
        if(key == GLFW_KEY_H && action == GLFW_PRESS)
            hud->show=!hud->show;            
            
        // Restart the level
        if(key == GLFW_KEY_R && action == GLFW_PRESS)
        {
            player->Reset();
            physics_handler->Reset();
        }
        
        // Player shoot action
        if(key == GLFW_KEY_SPACE)
        {              
            if(action == GLFW_PRESS)
            {
                // First time start drawing
                player->Draw();
            }
            else if(action == GLFW_RELEASE && player->drawing)
            {   
                // Shoot the arrow
                player->Shoot();
            }
        } 
        else
        {
            // If any key but space is pressed then release current drawing, arrow is not shot
            player->Release();
        }

        // we keep trace of the pressed keys
        // with this method, we can manage 2 keys pressed at the same time:
        // many I/O managers often consider only 1 key pressed at the time (the first pressed, until it is released)
        // using a boolean array, we can then check and manage all the keys pressed at the same time
        if(action == GLFW_PRESS) 
        {
            keys[key] = true;
        }
        else if(action == GLFW_RELEASE)
        {
            keys[key] = false;
        }
    }
    
    void apply(GLfloat deltaTime)
    {        
        // Player movement, classic WASD
        if(keys[GLFW_KEY_W])
            player->Move(FORWARD);
        if(keys[GLFW_KEY_S])
            player->Move(BACKWARD);     
        if(keys[GLFW_KEY_A])
            player->Move(LEFT);           
        if(keys[GLFW_KEY_D])
            player->Move(RIGHT);          

        // Player stop moving
        if(!keys[GLFW_KEY_W] && !keys[GLFW_KEY_S] && !keys[GLFW_KEY_A] && !keys[GLFW_KEY_D])
            player->Stop();
            
        // Player is drawing
        if(keys[GLFW_KEY_SPACE] && player->drawing)
        {            
            player->Hold();
            // When tired apply offset to mouse movement
            if(player->tired)
                mouse_moved(cursorX + player->tired_offset.x, cursorY + player->tired_offset.y);
        }
        
        // Rendering mode
        if (wireframe)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);              
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);              
    }
    
    // callback for mouse events
    void mouse_moved(GLFWwindow* window, double xpos, double ypos)
    {
        mouse_moved(xpos, ypos);
    }
    
    // centralize move movement
    void mouse_moved(double xpos, double ypos)
    {
        // we move the camera view following the mouse cursor
        // we calculate the offset of the mouse cursor from the position in the last frame
        // when rendering the first frame, we do not have a "previous state" for the mouse, so we set the previous state equal to the initial values (thus, the offset will be = 0)
        if(firstMouse)
        {
            lastX = xpos;
            lastY = ypos;
            firstMouse = false;
        }

        // we save the current cursor position in 2 global variables, in order to use the values in the keyboard callback function
        cursorX = xpos;
        cursorY = ypos;

        // offset of mouse cursor position
        GLfloat xoffset = xpos - lastX;
        GLfloat yoffset = lastY - ypos;

        // the new position will be the previous one for the next frame
        lastX = xpos;
        lastY = ypos;

        // we pass the offset to the Player class instance in order to update the rendering
        player->LookAt(xoffset, yoffset);
    }

private:
    Player* player;
    Hud* hud;
    PhysicsHandler* physics_handler;
            
    // boolean to activate/deactivate wireframe rendering
    GLboolean wireframe = GL_FALSE;
    
    // we initialize an array of booleans for each keybord key
    bool keys[1024] = { false };
    
    // we set the initial position of mouse cursor in the application window
    GLfloat lastX = WindowHelper::SCREEN_WIDTH/2, lastY = WindowHelper::SCREEN_HEIGHT/2;
    
    // we will use these value to "pass" the cursor position to the keyboard callback, in order to determine the bullet trajectory
    double cursorX, cursorY;

    // when rendering the first frame, we do not have a "previous state" for the mouse, so we need to manage this situation
    bool firstMouse = true;    
    
};

