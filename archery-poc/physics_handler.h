#pragma once

#include <utils/physics_v1.1.h>
#include "game_object.h"
#include "hud.h"

// Invoked by physics engine each step simulation time
void simulationTickCallback(btDynamicsWorld *dynamics_world, btScalar time_step);
// Store colliding objects and related contact points
std::map<const btCollisionObject*,std::vector<btManifoldPoint*> > collisions;

class PhysicsHandler
{
public:        

    PhysicsHandler(Hud* hud)
    {        
        this->physics  = Physics();
        // This function is invoked by the physics engine
        physics.dynamicsWorld->setInternalTickCallback(simulationTickCallback);
        this->hud = hud;
    }
    ~PhysicsHandler()
    {
    }
    
    // Wrap physics step simulation
    void stepSimulation(float time_step)
    {        
        physics.dynamicsWorld->stepSimulation(time_step < MAX_SEC_PER_FRAME ? time_step : MAX_SEC_PER_FRAME, 10);
    }
        
    // Wrap physics cleanup
    void Clear()
    {    
        physics.Clear();
    }        
    
    // Create a rigid body having the same position/size/orientation of specified game object. Created rigid body is linked to the game object.
    void CreateRigidBody(GameObject* go, int rigid_body_type, float mass, float friction, float restitution)
    {        
        CreateRigidBody(go, rigid_body_type, go->size, mass, friction, restitution);
    }
    
    // Create a rigid body with specified posizion/size; orientation is retrieved from specified game object. Created rigid body is linked to the game object.
    void CreateRigidBody(GameObject* go, int rigid_body_type, glm::vec3 rigid_body_size, float mass, float friction, float restitution)
    {                  
        btRigidBody* rigid_body = CreateRigitBody(rigid_body_type, go->position, rigid_body_size, go->rotation, mass, friction, restitution);                        
        go->rigid_body = rigid_body;
        // Store target references: this will be used on collision detection handling to understand if the arrow collided with a target.
        if(go->configuration.type == GameObject::TARGET)
        {            
            targets.push_back(go);
        }
    }
    
    // Create a rigid body with specified posizion/size/rotation.
    btRigidBody* CreateRigitBody(int type, glm::vec3 position, glm::vec3 size, glm::vec3 rotation, float mass, float friction, float restitution)
    {
        return physics.createRigidBody(type, position, size, rotation, mass, friction, restitution);   
    }
    
    // Handle arrow physics. This method is divided in two main part
    // 1. handle arrow collision
    // 2. handle arrow orientation while flying
    void HandleArrowPhysics(GameObject *arrow)
    {
        // Note that when a collision happens arrow rigid body is removed from arrow game object 
        if(arrow->rigid_body && arrow->rigid_body->getMotionState())
        {
            auto& arrow_contact_points = collisions[arrow->rigid_body];
            if (!arrow_contact_points.empty()) {
                // *******************************************************************************
                // HANDLE ARROW COLLISION
                
                // loop over targets to check if the arrow collided with one of them            
                for(GameObject* target : targets)
                {   
                    // Check whether current target was already hit
                    bool never_hit = std::find(hit_targets.begin(), hit_targets.end(), target->rigid_body) == hit_targets.end();                     
                    
                    // Search for contact points on current target
                    auto& target_contact_points = collisions[target->rigid_body];                    
                    if (never_hit && !target_contact_points.empty()) 
                    {             
                        // This will be passed to fragment shader to hightlight hit target
                        target->hightlight = true;
                        
                        // Select first contact point, extract local x and y coordinates
                        float hit_x, hit_y;
                        for(btManifoldPoint *contact_point : target_contact_points)
                        {
                            hit_x = contact_point->m_localPointA.getX();
                            hit_y = contact_point->m_localPointA.getY();
                            std::cout << "Hit points " << hit_x << ", " << hit_y << ", " << contact_point->m_localPointA.getZ() << std::endl; 
                            break;                                                         
                        }

                        // Calculate score: (radius - distance_from_center)/radius * 10 * distance_from_target
                        float radius = target->size.x / 2;
                        float distance_from_center = sqrt(pow(hit_x, 2) + pow(hit_y, 2));
                        float distance_from_target = (
                                btVector3(arrow->initial_position.x, arrow->initial_position.y, arrow->initial_position.z) - 
                                target->rigid_body->getCenterOfMassPosition()
                            ).length(); 
                        int score = (float) std::max(0.0f, (radius - distance_from_center)) / radius * 10 * distance_from_target;
                                                
                        // Update score
                        hud->UpdateScore(score, distance_from_target);
                        
                        // Store hit target 
                        hit_targets.push_back(target->rigid_body);      

                        // Debug logs
                        std::cout << "target hit! " 
                            << ", radius " << radius
                            << ", distance_from_center " << distance_from_center
                            << ", distance_from_target " << distance_from_target                            
                            << std::endl;                             
                        break;
                    }                            
                }  

                // If all targets are hit then game over message is shown
                if(hit_targets.size() == targets.size())
                {
                    // Show game over message
                    hud->GameOver();                                        
                }
                                   
                // Remove rigid body from arrow game object
                delete arrow->rigid_body->getMotionState();
                physics.dynamicsWorld->removeCollisionObject(arrow->rigid_body);
                delete arrow->rigid_body;
                arrow->rigid_body = nullptr;
            }
            else 
            {                
                // *******************************************************************************
                // HANDLE ARROW ORIENTATION WHILE FLYING
                
                // Retrieve arrow rotation matrix from physics engine
                btTransform transform;
                arrow->rigid_body->getMotionState()->getWorldTransform(transform);
                btMatrix3x3 arrow_rot_matrix = transform.getBasis();        
                
                // Create a vector with the same orientation of flying arrow
                // z is -1 because must poit inside the screen (right hand, z is positive outside the screen)
                btVector3 arrow_vector = arrow_rot_matrix * btVector3(0, 0, -1);
                // Vector angle on YZ plane, 0 degree when points up, 180 when points down
                float arrow_angle = atan2(arrow_vector.getZ(), arrow_vector.getY());
                                                                                                     
                // Retrieve velocity vector from physic engine
                btVector3 velocity_vector_orig = arrow->rigid_body->getLinearVelocity();      
                btVector3 velocity_vector = btVector3(velocity_vector_orig.getX(), velocity_vector_orig.getY(), velocity_vector_orig.getZ());
                velocity_vector.normalize();
                // Vector angle on YZ plane, 0 degree when points up, 180 when points down
                float velocity_angle = atan2(velocity_vector.getZ(), velocity_vector.getY());        
                
                // Velocity vector angle ranges can be 0/180 or 0/-180: velocity angle never turn back.
                // Arrows vector, otherwise, can turn back because of applied torque force: if this happens we have to adjust angle values. 
                // If initial range is 0/180 then turn back angle range will be -180/0: this must be adjusted to 180/360. 
                // If initial range is 0/-180 then turn back angle range will be 180/0: this must be adjusted to -180/-360. 
                // Velocity angle is on 2° or 3° quadrants (0/180); arrow turned back so arrow angle is on 4° or 1° quadrants (-180/0): adjust arrow angle to 180/360
                if(velocity_angle > 0 && arrow_angle < 0)
                {
                    arrow_angle += 2 * glm::pi<float>();  
                } 
                // Velocity angle is on 1° or 4° quadrants (0/-180), arrow turned back so arrow angle is on 3° or 2° quadrants (180/0): adjust arrow angle to -180/-360
                else if(velocity_angle < 0 && arrow_angle > 0)
                {
                    arrow_angle -= 2 * glm::pi<float>();  
                }
                
                // Difference between velocity and angle arrow
                float angle_diff = velocity_angle - arrow_angle;
                                
                // Apply torque on the arrow with a force that depend on angles divergence and velocity strenght
                if(abs(angle_diff) >= MIN_ANGLE_OFFSET)   
                {             
                    // Torque force to be applied: vector_len * angle_diff * TORQUE_COEFF
                    int sign = velocity_angle > 0 ? -1 : 1;
                    float force = sign * (velocity_vector_orig.length() * angle_diff * TORQUE_COEFF);
                    
                    // Calculate local torque vector and multiply by arrow rotation matrix in order to find world torque vector
                    btVector3 local_torque = btVector3(force, 0, 0);
                    btVector3 world_torque = arrow_rot_matrix * local_torque;
                    arrow->rigid_body->applyTorque(world_torque);
                    
                    // Debug log, enable when needed
//                    if(abs(velocity_vector_orig.length()) > 1)
//                    {                        
//                       std::cout 
//                       << "vel len: " << velocity_vector_orig.length() << ", vel ang: " << glm::degrees(velocity_angle) << ", arr ang: " << glm::degrees(arrow_angle) 
//                       << ", angle_diff: " << glm::degrees(angle_diff)  
//                       << ", force " << force
//                       << ", vel (" << velocity_vector.getZ() << ", " << velocity_vector.getY() << "), arr (" << arrow_vector.getZ() << ", " << arrow_vector.getY() << ")"
//                       << std::endl;                   
//                    }
                }                     
            }             
        }
    }
    
    void Reset()
    {
        // Clear hit target vector
        hit_targets.clear();

        // Remove hit target highlight
        for(GameObject* target : targets)
        { 
            target->hightlight = false;
        }
    }
    
private:
    
    const float TORQUE_COEFF = 0.015f;
    const float MIN_ANGLE_OFFSET = glm::radians(1.0f); 
    const float MAX_SEC_PER_FRAME = 1.0f / 60.0f;

    Physics physics;    
    Hud* hud;
    
    // All targets and hit targets
    vector<GameObject*> targets;
    vector<btRigidBody*> hit_targets;    
};


void simulationTickCallback(btDynamicsWorld *dynamics_world, btScalar time_step) {
        // Cleanup old collisions
        collisions.clear();
        
        // Loop over collisions and store both collided rigid bodies references and contact points 
        int collision_num = dynamics_world->getDispatcher()->getNumManifolds();
        for (int i = 0; i < collision_num; i++) {            
            // Collided bodies
            btPersistentManifold *collided_bodies = dynamics_world->getDispatcher()->getManifoldByIndexInternal(i);
            auto *body0 = collided_bodies->getBody0();
            auto *body1 = collided_bodies->getBody1();                                    
            // Pointer to contact point vector associated to body0 key
            auto& contact_points1 = collisions[body0];
            // Pointer to contact point vector associated to body1 key
            auto& contact_points2 = collisions[body1];
            // Loop over contact points
            int contact_num = collided_bodies->getNumContacts();
            for (int j = 0; j < contact_num; j++) {
                btManifoldPoint& contact_point = collided_bodies->getContactPoint(j);
                contact_points1.push_back(&contact_point);
                contact_points2.push_back(&contact_point);                      
            }
        }    
    }

