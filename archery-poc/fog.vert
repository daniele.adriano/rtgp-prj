#version 330 core

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec2 in_texcoord;

uniform mat4 modelMatrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out vec3 world_position;
out vec3 world_normal;
out vec2 interp_UV;
out vec4 view_position;

void main() {

	// Vertex position in world coordinates
	world_position = (modelMatrix * vec4(in_position,1)).xyz;

	// Vertex normal position in world coordinates
	world_normal = normalize(mat3(modelMatrix) * in_normal);

	// Interpolated UV coordinates
	interp_UV = in_texcoord;
	 
	// Vertex position in view coorinates
	view_position = view_matrix * modelMatrix * vec4(in_position,1);

	// Final position with projection matrix
	gl_Position = projection_matrix * view_position;

}
