#pragma once

namespace TextureHelper
{
    
    GLint LoadTexture(const char* path)
    {
        GLuint textureImage;
        int w, h, channels;
        unsigned char* image;
        image = stbi_load(path, &w, &h, &channels, 0);

        if (image == nullptr)
            std::cout << "Failed to load texture!" << std::endl;

        glGenTextures(1, &textureImage);
        glBindTexture(GL_TEXTURE_2D, textureImage);
        // 3 channels = RGB ; 4 channel = RGBA
        if (channels==3)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
            // we set how to consider UVs outside [0,1] range
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        }
        else if (channels==4)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
            // to remove black border on image with alpha channel
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }        
        
        glGenerateMipmap(GL_TEXTURE_2D);
        // we set the filtering for minification and magnification
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);

        // we free the memory once we have created an OpenGL texture
        stbi_image_free(image);                

        return textureImage;
    }
    
    GLint LoadTextureCube(string path)
    {
        GLuint textureImage;
        int w, h;
        unsigned char* image;
        string fullname;

        glGenTextures(1, &textureImage);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, textureImage);

        // we use as convention that the names of the 6 images are "posx, negx, posy, negy, posz, negz", placed at the path passed as parameter
        //POSX
        fullname = path + std::string("posx.jpg");
        image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
        if (image == nullptr)
            std::cout << "Failed to load texture!" << std::endl;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        // we free the memory once we have created an OpenGL texture
        stbi_image_free(image);
        //NEGX
        fullname = path + std::string("negx.jpg");
        image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
        if (image == nullptr)
            std::cout << "Failed to load texture!" << std::endl;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        // we free the memory once we have created an OpenGL texture
        stbi_image_free(image);
        //POSY
        fullname = path + std::string("posy.jpg");
        image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
        if (image == nullptr)
            std::cout << "Failed to load texture!" << std::endl;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        // we free the memory once we have created an OpenGL texture
        stbi_image_free(image);
        //NEGY
        fullname = path + std::string("negy.jpg");
        image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
        if (image == nullptr)
            std::cout << "Failed to load texture!" << std::endl;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        // we free the memory once we have created an OpenGL texture
        stbi_image_free(image);
        //POSZ
        fullname = path + std::string("posz.jpg");
        image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
        if (image == nullptr)
            std::cout << "Failed to load texture!" << std::endl;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        // we free the memory once we have created an OpenGL texture
        stbi_image_free(image);
        //NEGZ
        fullname = path + std::string("negz.jpg");
        image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
        if (image == nullptr)
            std::cout << "Failed to load texture!" << std::endl;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        // we free the memory once we have created an OpenGL texture
        stbi_image_free(image);

        // we set the filtering for minification and magnification
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // we set how to consider the texture coordinates outside [0,1] range
        // in this case we have a cube map, so
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

        return textureImage;
    }   


};

