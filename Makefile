.PHONY: clean All

All:
	@echo "----------Building project:[ archery-poc - Debug ]----------"
	@cd "archery-poc" && "$(MAKE)" -f  "archery-poc.mk" && "$(MAKE)" -f  "archery-poc.mk" PostBuild
clean:
	@echo "----------Cleaning project:[ archery-poc - Debug ]----------"
	@cd "archery-poc" && "$(MAKE)" -f  "archery-poc.mk" clean
